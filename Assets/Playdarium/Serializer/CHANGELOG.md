# Changelog

---

## [v1.2.10](https://gitlab.com/pd-packages/serialize-property-serializer/-/tags/v1.2.10)

### Fixed

- Skip fields with Null value in class and struct

---

## [v1.2.10](https://gitlab.com/pd-packages/serialize-property-serializer/-/tags/v1.2.10)

### Fixed

- Skip fields with NonSerialized attribute in class and struct

---

## [v1.2.9](https://gitlab.com/pd-packages/serialize-property-serializer/-/tags/v1.2.9)

### Fixed

- Get fields from a type when class or struct serialized

---

## [v1.2.8](https://gitlab.com/pd-packages/serialize-property-serializer/-/tags/v1.2.8)

### Fixed

- Enum properties serialization

---

## [v1.2.6](https://gitlab.com/pd-packages/serialize-property-serializer/-/tags/v1.2.6)

### Changed

- Package name `com.playdarium.unity.serialize-property-serializer`
- Move to GitLab

### Fixed

- Avoid throwing exception during serialized properties

---

## [v1.2.5](https://github.com/Playdarium/serialize-property-serializer/releases/tag/v1.2.5)

### Added

- Changelog

### Changed

- Package tool dependency to `com.playdarium.package-tool`

---

## [v0.0.0](https://github.com/Playdarium/serialize-property-serializer/releases/tag/v0.0.0)

### Added

- For new features.

### Changed

- For changes in existing functionality.

### Deprecated

- For soon-to-be removed features.

### Removed

- For now removed features.

### Fixed

- For any bug fixes.

### Security

- In case of vulnerabilities.
