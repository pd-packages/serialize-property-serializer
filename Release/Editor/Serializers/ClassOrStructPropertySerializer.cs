using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Playdarium.Serializer.Serializers
{
	public class ClassOrStructPropertySerializer : IPropertySerializer
	{
		private const BindingFlags FIELD_BINDING = BindingFlags.Instance
		                                           | BindingFlags.Public
		                                           | BindingFlags.NonPublic;

		public bool CanSerialize(Type type) => type.IsClass || type.IsValueType;

		public void Serialize(object value, SerializedProperty property)
		{
			var fieldsInfo = GetFields(value.GetType());
			var serializedFields = fieldsInfo
				.Where(f => !f.IsDefined(typeof(NonSerializedAttribute)))
				.Where(f => f.IsPublic || f.IsPrivate && f.IsDefined(typeof(SerializeField)));
			foreach (var serializedField in serializedFields)
			{
				var fieldValue = serializedField.GetValue(value);
				if (fieldValue == null)
					continue;

				var propertyRelative = property.FindPropertyRelative(serializedField.Name);
				try
				{
					SerializedPropertySerializer.SerializeValueProperty(fieldValue, propertyRelative);
				}
				catch (Exception e)
				{
					Debug.LogException(e);
					Debug.LogError(
						$"[{nameof(SerializedPropertySerializer)}] Can't serialize property {serializedField.FieldType} Name: {serializedField.Name}");
				}
			}
		}

		private List<FieldInfo> GetFields(Type type)
		{
			var result = new List<FieldInfo>();

			while (type != null)
			{
				result.AddRange(type.GetFields(FIELD_BINDING));
				type = type.BaseType;
			}

			return result;
		}
	}
}